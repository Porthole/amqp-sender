FROM node:15

WORKDIR /opt/app
COPY . .

RUN npm i

CMD npm start

